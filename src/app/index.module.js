(function() {
  'use strict';

  angular
    .module('tcgDeckBuilder', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router', 'ngMaterial', 'toastr']);

})();
