(function() {
  'use strict';

  angular
    .module('tcgDeckBuilder')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
