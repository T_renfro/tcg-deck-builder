(function () {
    'use strict';

    angular
        .module('tcgDeckBuilder')
        .config(routerConfig);

    /** @ngInject */
    function routerConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })

            .state('deck-building', {
                url: "/deck-building",
                templateUrl: "app/main/deck-building.html",
                controller: 'MainController',
                controllerAs: 'main'
            })

            .state('browse', {
                url: "/browse",
                templateUrl: "app/main/browse.html",
                controller: 'MainController',
                controllerAs: 'main'
            })

            .state('card-details', {
                url: "/card-details",
                templateUrl: "app/main/card-details.html",
                controller: 'MainController',
                controllerAs: 'main'
            });
        $urlRouterProvider.otherwise('/');
    }

})();
